(function() {

	var $form = $('.form'); // Add/edit form

	// On common add/edit form submit

	$form.on('submit', function(e) {
		e.preventDefault();
		$id = parseInt($form.find('.id').val());
		if ($id === 0) { // Add item
			addItem();
		}
		else { // Edit item
			editItem();
		}
		closeForm();
		location.reload(); // Just reload page instead of bulky jquery dom manipulations
	});

	// Common click event listener on page
	// Instead of multiple event listeners for every button

	$('.wrapper').on('click', function(e) {
		
		var $target = $(e.target);

		switch(true)
		{
			case $target.is('.add'):
				onAddButtonClick($target);
				break;

			case $target.is('.addsub'):
				onAddSubButtonClick($target);
				break;

			case $target.is('.edit'):
				onEditButtonClick($target);
				break;

			case $target.is('.delete'):
				onDeleteButtonClick($target);
				break;

			case $target.is('.close'):
				closeForm();
				break;
		}

	});

	// Functions that used in common click event listener

	function onAddButtonClick($target) {
		$form.find('input[type="text"], textarea').val('');
		$form.find('.id').val('0');
		$form.find('.parent_id').val('0');
		$form.css('display', 'block');
	}

	function onAddSubButtonClick($target) {
		$form.find('input[type="text"], textarea').val('');
		$form.find('.id').val('0');
		var $li = $target.closest('li');
		$form.find('.parent_id').val($li.attr('data-id'));
		$form.css('display', 'block');
	}

	function onEditButtonClick($target) {
		var $li = $target.closest('li');
		var $id = $li.attr('data-id');
		var $liParent = $li.parents('li');
		var $parentId = ($liParent.length !== 0) ? $liParent.attr('data-id') : "0";
		$form.find('.id').val($id);
		$form.find('.parent_id').val($parentId);
		getDataByIDAndFillForm($id);
		$form.css('display', 'block');
	}

	function onDeleteButtonClick($target) {
		if (confirm("Are you sure want to delete this item AND ALL IT'S CHILDREN?")) {
			var $li = $target.closest('li');
			var $id = $li.attr('data-id');
			$.ajax({
				method: 'POST',
				url: '/pagesPseudoREST/delete/',
				data: { id: $id },
				success: function(result) {
					if (result === 'ok') {
						alert('Page successfully removed!');
						$li.remove();
					}
					else {
						alert(result);
					}
				},
				error: onAJAXError
			})
		}
	}

	// Close form

	function closeForm() {
		$form.css('display', 'none');	
	}

	// Get item URL, text, etc by ID from server and put these data to form
	function getDataByIDAndFillForm(id) {
		var data = [];
		$.ajax({
			type: 'POST',
			url: '/pagesPseudoREST/get_by_id/',
			data: {id: id},
			success: function(result) {
				data = JSON.parse(result);
				$form.find('.url').val(data['url']);
				$form.find('.title').val(data['title']);
				$form.find('.text').val(data['text']);
			},
			error: onAJAXError
		})
	}

	// Add/edit item functions

	function addItem(parentId) {
		var data = $('.form :input').serializeArray();
		console.log(data);
		$.ajax({
			method: 'POST',
			url: '/pagesPseudoREST/add/',
			data: data,
			success: function(result) {
				if (result === 'ok') {
					alert('Page successfully added!');
				}
				else {
					alert(result);
				}
			},
			error: onAJAXError
		})
	}

	function editItem() {
		var data = $('.form :input').serializeArray();
		console.log(data);
		$.ajax({
			method: 'POST',
			url: '/pagesPseudoREST/update/',
			data: data,
			success: function(result) {
				if (result === 'ok') {
					alert('Page successfully edited!');
				}
				else {
					alert(result);
				}
			},
			error: onAJAXError
		})
	}

	// AJAX Error handler
	function onAJAXError(xhr, text, errorThrown) {
		alert('error: ' + errorThrown + ' ' + text);
	}

	$('.sortItem').sortable({
		items: ".pageItem",
		connectWith: ".sortItem",
		update: function(e, ui) {
			if (this === ui.item.parent()[0]) {
				var $li = ui.item;
				var $id = $li.attr('data-id');
				var $liParent = $li.parents('li');
				var $parentId = ($liParent.length !== 0) ? $liParent.attr('data-id') : "0";
				$.ajax({
					method: 'POST',
					url: '/pagesPseudoREST/change_parent/',
					data: { id: $id, parent_id: $parentId },
					success: function(result) {
						if (result === 'ok') {
							alert('Parent successfully changed!');
						}
						else {
							alert(result);
						}
					},
					error: onAJAXError
				})
			}
		}
	});

})();
