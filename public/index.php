<?php 

	spl_autoload_register(function($className) {
		$dir = "../src/" . str_replace("\\", "/", $className) . ".php";
		if (file_exists($dir)) {
			include $dir;
		}
	});

	session_start();

	try {
		$app = new App();
		$res = $app->run();
		echo $res;
	}
	catch (Exception $e) {
		echo $e->getMessage();
	}
	
