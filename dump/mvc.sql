-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 07 2016 г., 20:42
-- Версия сервера: 5.5.52-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mvc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `text` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `title`, `url`, `text`) VALUES
(1, 0, 'заголовок', 'titleeeeee', 'textуууууууууу'),
(2, 0, 'eeeeerock', 'titleeeeee', 'sfdfadgdsg'),
(3, 2, 'title123', 'titleeeeee/abcd234567890', 'textывфывфывыЕЕЕЕr'),
(6, 0, 'Главная', 'main', 'Это главная страница'),
(7, 0, 'О нас', 'about', 'Это страница "О нас"'),
(8, 0, 'Контакты', 'contacts', 'Это страница контактов'),
(9, 1, 'asdsffs', 'titleeeeee/kkkkk', 'EDFKSDLFKSD'),
(10, 0, 'aaa', 'KEEEEK', 'fasfasfasf'),
(11, 1, 'ФФФФ', 'titleeeeee/hhhhh', 'ффф'),
(12, 2, 'пооовар', 'titleeeeee/titleeeeee/kuk', 'повар спрашивает повара');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
