<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Страница</title>
</head>
<body>
	<?=\Framework\View::render('pages_common/menu'); ?>
	<ul class="first_level_pages">
		<?php foreach($firstLevel as $item): ?>
			<li><a href="/<?=$item['url']?>"><?=$item['title']?></a></li>
		<?php endforeach; ?>
	</ul>
	<h3><?=$title?></h3>
	<div><?=$text?></div>
	<ul>
		<?php foreach($subPages as $item): ?>
			<li><a href="/<?=$item['url']?>"><?=$item['title']?></a></li>
		<?php endforeach; ?>
	</ul>
</body>
</html>
