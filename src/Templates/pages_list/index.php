<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Список</title>
	<link rel="stylesheet" href="/assets/css/style.css">
	<script src="/assets/js/jquery-2.1.4.min.js"></script>
	<script src="/assets/js/jquery-ui.min.js"></script>
</head>
<body>
	<div class="wrapper">
		<h1>Pages list</h1>
		<button class="add">add...</button>
		<a href="/login/logout">Logout</a>
		<ul class="list sortItem">
			<?=$items?>
		</ul>
		<?=\Framework\View::render('pages_list/form')?>
	</div>
	<script src="/assets/js/script.js"></script>
</body>
</html>
