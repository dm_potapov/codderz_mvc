<?php

	namespace App\Controllers;

	class PagesPseudoREST extends \Framework\Controller {

		private $model;

		function __construct() {
			if (!\Framework\Auth::logged_in()) {
				die("You aren't logged in!");
			}
			$this->model = new \App\ORM\Page;
		}

		function action_add() {
			if ((int)$_POST['parent_id'] !== 0) {
				$item = $this->model->get(array('id' => $_POST['parent_id']))[0];
				$_POST['url'] = $item['url'] . '/' . $_POST['url'];
			}
			return $this->model->put($_POST);
		}

		function action_update() {
			$id = $_POST['id'];
			unset($_POST['id']);
			if ((int)$_POST['parent_id'] !== 0) {
				$parentId = $_POST['parent_id'];
				$parentUrl = $this->model->get(array('id' => $parentId))[0]['url'];
				$_POST['url'] = $parentUrl . '/' . $_POST['url'];
			}
			return $this->model->update($_POST, array('id' => $id));
		}

		function action_delete() {
			$id = $_POST['id'];
			$this->model->delete(array('id' => $id));
			return $this->model->delete(array('parent_id' => $id));
		}

		function action_get_by_id() {
			$id = $_POST['id'];
			$elem = $this->model->get(array('id' => $id))[0];
			$elemUrlParts = explode('/', $elem['url']);
			$elem['url'] = $elemUrlParts[count($elemUrlParts)-1];
			return json_encode($elem);
		}

		function action_change_parent() {
			$id = $_POST['id'];
			$parentId = $_POST['parent_id'];
			// I know it looks very bad but I tried to write this only with my microORM
			$itemUrl = $this->model->get(array('id' => $id))[0]['url'];
			$itemUrlParts = explode('/', $itemUrl);
			if ((int)$parentId !== 0) {
				$parentUrl = $this->model->get(array('id' => $parentId))[0]['url'];
				$itemUrl = $parentUrl . '/' . $itemUrlParts[count($itemUrlParts)-1];
			}
			else {
				$itemUrl = $itemUrlParts[count($itemUrlParts)-1];
			}
			return $this->model->update(array('parent_id' => $parentId,
											'url' => $itemUrl),
										array('id' => $id));
		}

	}
