<?php

	namespace App\Controllers;

	class Pages extends \Framework\Controller {

		public function action_index() {
			if (!\Framework\Auth::logged_in()) {
				header("location: /login");
			}
			$model = new \App\ORM\Page;
			$pages = $model->get();
			$html = $this->recursiveListParse($pages, 0);
			return \Framework\View::render('pages_list/index', 
											array('items' => $html));
		}

		private function recursiveListParse($pages, $parent) {
			$html = "";
			foreach($pages as $page) {
				if ((int)$page['parent_id'] === $parent) {
					$page['subItems'] = $this->recursiveListParse($pages, (int)$page['id']);
					$html .= \Framework\View::render('pages_list/item', $page);
				}
			}
			return $html;
		}

		public function action_get_page() {
			$model = new \App\ORM\Page;
			$page = $model->get(array('url' => trim($_SERVER['REQUEST_URI'], "/")));
			$res = "";
			if (count($page) !== 0) {
				$page = $page[0];
				$id = $page['id'];
				$subPages = $model->get(array('parent_id' => $id));
				$firstLevelList = $model->get(array('parent_id' => 0));
				$res = \Framework\View::render('page', 
								array_merge(array('subPages' => $subPages,
													'firstLevel' => $firstLevelList), 
												$page));
			}
			else {
				$res = "<h1>404 not found</h1>";
			}
			return $res;
		}

	}
