<?php

	namespace App\Controllers;

	class Cool extends \Framework\Controller {

		public function action_index() {
			$_SERVER['REQUEST_URI'] = "main";
			return (new Pages)->action_get_page();
		}

		public function action_kek() {
			return \Framework\View::render('kek', array('kek' => 12345));
		}

	}
