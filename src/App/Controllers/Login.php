<?php

	namespace App\Controllers;

	class Login extends \Framework\Controller {

		public function action_index() {
			if (\Framework\Auth::logged_in()) {
				header('location: /pages');
				die;
			}
			return \Framework\View::render('login');
		}

		public function action_check() {
			$user = (isset($_POST['user'])) ? $_POST['user'] : "";
			$pass = (isset($_POST['pass'])) ? $_POST['pass'] : "";
			if (\Framework\Auth::login($user, $pass)) {
				header('location: /pages');
				die;
			}
			else {
				die;
				header('location: /login/index/?incorrect=1');
				die;
			}
		}

		public function action_logout() {
			\Framework\Auth::logout();
			header('location: /login');
			die;
		}

	}
