<?php

	class App {

		public function run() {

			$controllerName = "\App\Controllers\\";
			$method = "action_";

			if ($_SERVER['REQUEST_URI'] === "/") {
				$controllerName .= \Framework\Config::get('defaultController->name');
				$method .= \Framework\Config::get('defaultController->action');
			}
			else {
				$url = trim($_SERVER['REQUEST_URI'], "/");
				$urlParts = explode("/", $url);
				$controllerName .= ucfirst($urlParts[0]);
				$method .= isset($urlParts[1]) ? $urlParts[1] : "index";
				if (!class_exists($controllerName)) {
					$controllerName = '\App\Controllers\Pages';
					$method = 'action_get_page';
				}
				else {	
					if (!method_exists($controllerName, $method)) {
						throw new Exception("Action $method does not exist!");
					}
				}
			}

			$controller = new $controllerName;
			$res = $controller->$method();

			return $res;
		}

	}
