<?php

	namespace Framework;

	abstract class ORM {

		protected $modelName;
		private $connection;
		private $tableName;

		public function __construct() {
			extract(get_object_vars(\Framework\Config::get('db')));
			$connString = "mysql:dbname=$dbName;host=$dbHost;charset=utf8";
			$this->connection = new \PDO($connString, $dbLogin, $dbPassword);
			$this->tableName = $this->modelName . 's';
		}

		public function get($where = array()) {
			$queryStr = "SELECT * FROM `$this->tableName` ";

			$queryStr .= $this->getWhereString($where);
			
			$pdo = $this->connection->query($queryStr);

			$res = array();

			foreach($pdo as $row) {
				$res[] = $row;
			}

			return $res;
		}

		public function put($values = array()) {
			$valuesWithParametredKeys = array(); // $values copy with ':key' keys for PDO->execute()
			foreach ($values as $key => $value) {
				$valuesWithParametredKeys[':'.$key] = $value;
			}
			$fieldsStr = implode("`,`", array_keys($values));
			$parametredValuesStr = implode(",", array_keys($valuesWithParametredKeys)); // for PDO->prepare()
			$queryStr = "INSERT INTO `$this->tableName` (`$fieldsStr`) VALUES ($parametredValuesStr)";
			$obj = $this->connection->prepare($queryStr);
			$obj->execute($valuesWithParametredKeys);
			return 'ok';
		}

		public function update($values = array(), $where = array()) {
			if ($values === array()) {
				throw new Exception("Values to update aren't set!");
			}

			$valuesWithParametredKeys = array();
			foreach ($values as $key => $value) {
				$valuesWithParametredKeys[':'.$key] = $value;
			}

			$queryStr = "UPDATE `$this->tableName` SET ";
			foreach ($values as $key => $value) {
				$queryStr .= "`$key` = :$key,";
			}
			$queryStr = substr($queryStr, 0, strlen($queryStr) - 1);
			$queryStr .= (" " . $this->getWhereString($where));
			$obj = $this->connection->prepare($queryStr);
			$obj->execute($valuesWithParametredKeys);
			return 'ok';
		}

		public function delete($where = array()) {
			if ($where === array()) {
				throw new Exception("Criterias for deleting aren't set!");
			}

			$queryStr = "DELETE FROM `$this->tableName` ";
			$queryStr .= $this->getWhereString($where);
			$this->connection->query($queryStr);

			return 'ok';
		}

		private function getWhereString($where = array()) {
			$whereStr = "WHERE 1=1 ";
			foreach($where as $key => $value) {
				$whereStr .= "AND `$key` = '$value' ";
			}
			return $whereStr;
		}

	}
