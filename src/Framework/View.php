<?php

	namespace Framework;

	class View {

		public static function render($dir, $params = array()) {
			extract($params);
			ob_start();
			include('../src/Templates/' . $dir . '.php');
			$html = ob_get_clean();
			return $html;
		}
	}
