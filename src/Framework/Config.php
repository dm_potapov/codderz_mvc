<?php

	namespace Framework;

	class Config {

		public static function get($params) {
			$keys = explode("->", $params);
			$json = file_get_contents('../src/config.json');
			$config = json_decode($json);
			foreach ($keys as $key) {
				$config = $config->$key;
			}
			return $config;
		}

	}
