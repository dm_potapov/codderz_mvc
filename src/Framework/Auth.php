<?php

	namespace Framework;

	class Auth {

		public static function logged_in() {
			return isset($_SESSION['user']);	
		}

		public static function login($user, $pass) {
			$hash = hash(Config::get('auth->algo'), $pass);
			$success = false;
			if (Config::get('auth->user') === $user &&
				Config::get('auth->passHash') === $hash) {
				$_SESSION['user'] = $user;
				$success = true;
			}

			return $success;
		}

		public static function logout() {
			session_destroy();
		}

	}
